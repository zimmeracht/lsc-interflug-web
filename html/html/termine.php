<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

<head>
  <title>Termine</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="Generator" content="NetObjects Fusion 9 for Windows">
  <link rel="stylesheet" type="text/css" href="../html/fusion.css">
  <link rel="stylesheet" type="text/css" href="../html/style.css">
  <link rel="stylesheet" type="text/css" href="../html/site.css">

  <style type="text/css">
        body {
            margin: 0px auto;
            width: 974px;
        }
        div#LayoutLYR {
            float: left;
            position: absolute;
        }
        div#TextImpress {
            top: 600px
        }

        div#BildFacebook {
            top: 600px
        }

        div#TextFacebook {
            top: 600px
        }

        div#BildYoutube {
            top: 600px
        }

        div#TextYoutube {
            top: 600px
        }
  </style>
</head>

<body>
  <div id="LayoutLYR">
    <table border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <table border="0" cellspacing="0" cellpadding="0" width="974">
            <tr valign="top" align="left">
              <td width="4" height="79">
                <img src="../assets/images/autogen/clearpixel.gif" width="4" height="1" border="0" alt="">
              </td>
              <td width="970">
                <img id="Bild6" height="79" width="970" src="../assets/images/Luftsportclub_INTERFLUG_Berlin_e.V..jpg" border="0"
                  alt="Luftsportclub INTERFLUG Berlin e.V." title="Luftsportclub INTERFLUG Berlin e.V.">
              </td>
            </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" width="974">
            <tr valign="top" align="left">
              <td height="34" width="974">
                <div id="ExternerHTMLCode1Color">
                  <table width="974" height="34">
                    <tr align="left" valign="top">
                      <td>

                        <img src='../assets/images/empty.gif' width='1' alt='' height='1' id='TrackPath_m1' style='display:none'>
                        <script src="../assets/Navi_scr.js" type="text/javascript">
                        </script>

                        <table id="m1mainSXMenu2" cellspacing="1" cellpadding="5" style=";width:972px">
                          <tr style="text-align:center">
                            <td onmouseover="chgBg(m1,'m1tlm0',3);exM(m1,'none','',event)" onmouseout="chgBg(m1,'m1tlm0',0,1)" id="m1tlm0" onmousedown="f58('m1tlm0a')"
                              style="background-color:#F00000;" class="m1mit">
                              <a id="m1tlm0a" class="m1CL0" href="/index.html">Home</a>
                            </td>
                            <td onmouseover="chgBg(m1,'m1tlm1',3);exM(m1,'none','',event)" onmouseout="chgBg(m1,'m1tlm1',0,1)" id="m1tlm1" onmousedown="f58('m1tlm1a')"
                              style="background-color:#F00000;" class="m1mit">
                              <a id="m1tlm1a" class="m1CL0" href="/html/ubers_segelfliegen.html">&Uuml;bers Segelfliegen</a>
                            </td>
                            <td onmouseover="chgBg(m1,'m1tlm2',3);exM(m1,'m1mn1','m1tlm2',event)" onmouseout="chgBg(m1,'m1tlm2',0);coM(m1,'m1mn1')" id="m1tlm2"
                              style="background-color:#F00000;" class="m1mit">
                              <a id="m1tlm2a" class="m1CL0" href="javascript:void(0);">Unser Verein</a>
                            </td>
                            <td onmouseover="chgBg(m1,'m1tlm3',3);exM(m1,'m1mn2','m1tlm3',event)" onmouseout="chgBg(m1,'m1tlm3',0);coM(m1,'m1mn2')" id="m1tlm3"
                              style="background-color:#F00000;" class="m1mit">
                              <a id="m1tlm3a" class="m1CL0" href="javascript:void(0);">Flugzeuge & Technik</a>
                            </td>
                            <td onmouseover="chgBg(m1,'m1tlm4',3);exM(m1,'m1mn3','m1tlm4',event)" onmouseout="chgBg(m1,'m1tlm4',0);coM(m1,'m1mn3')" id="m1tlm4"
                              style="background-color:#F00000;" class="m1mit">
                              <a id="m1tlm4a" class="m1CL0" href="javascript:void(0);">Mitglied werden</a>
                            </td>
                            <td onmouseover="chgBg(m1,'m1tlm5',3);exM(m1,'m1mn4','m1tlm5',event)" onmouseout="chgBg(m1,'m1tlm5',0);coM(m1,'m1mn4')" id="m1tlm5"
                              style="background-color:#F00000;" class="m1mit">
                              <a id="m1tlm5a" class="m1CL0" href="javascript:void(0);">Segelflug erleben</a>
                            </td>
                            <td onmouseover="chgBg(m1,'m1tlm6',3);exM(m1,'m1mn5','m1tlm6',event)" onmouseout="chgBg(m1,'m1tlm6',0);coM(m1,'m1mn5')" id="m1tlm6"
                              style="background-color:#F00000;" class="m1mit">
                              <a id="m1tlm6a" class="m1CL0" href="javascript:void(0);">Unser Flugplatz</a>
                            </td>
                            <td onmouseover="chgBg(m1,'m1tlm7',3);exM(m1,'none','',event)" onmouseout="chgBg(m1,'m1tlm7',0,1)" id="m1tlm7" onmousedown="f58('m1tlm7a')"
                              style="background-color:#F00000;" class="m1mit">
                              <a id="m1tlm7a" class="m1CL0" href="/html/kontakt.html">Kontakt</a>
                            </td>

                          </tr>
                        </table>
                        <noscript>
                          <a href="/index.html">Home</a> |
                          <a href="/html/ubers_segelfliegen.html">&Uuml;bers Segelfliegen</a> |
                          <a href="/html/termine.php">Termine</a> |
                          <a href="/html/fliegerjugend.html">Fliegerjugend</a> |
                          <a href="/html/flieger-blog.html">Flieger-Blog</a> |
                          <a href="/html/bestleistungen.html">Bestleistungen</a> |
                          <a href="/html/vorstand.html">Vorstand</a> |
                          <a href="/html/puchacz_1656.html">Puchacz 1656</a> |
                          <a href="/html/puchacz_2655.html">Puchacz 2655</a> |
                          <a href="/html/bocian.html">Bocian</a> |
                          <a href="/html/acro.html">Acro</a> |
                          <a href="/html/ls_7.html">LS 7</a> |
                          <a href="/html/asw_15.html">ASW 15</a> |
                          <a href="/html/k8.html">K8</a> |
                          <a href="/html/wilga.html">Wilga</a> |
                          <a href="/html/winde_h4.html">Winde H4</a> |
                          <a href="/html/aufnahmeunterlagen.html">Aufnahmeunterlagen</a> |
                          <a href="/html/gebuhrenordnung.html">Geb&uuml;hrenordnung</a> |
                          <a href="/html/satzung.html">Satzung</a> |
                          <a href="/html/mitfliegen.html">Mitfliegen</a> |
                          <a href="/html/selbst_fliegen.html">Selbst fliegen</a> |
                          <a href="/html/schnupperfliegen.html">Schnupperkurs</a> |
                          <a href="/html/anfahrt.html">Anfahrt</a> |
                          <a href="/html/flugplatzdaten.html">Flugplatzdaten</a> |
                          <a href="/html/gastronomie.html">Gastronomie</a> |
                          <a href="/html/kontakt.html">Kontakt</a> |
                        </noscript>

                      </td>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
          </table>
          <table border="0" cellspacing="0" cellpadding="0" width="974">
            <tr valign="top" align="left">
              <td width="4" height="17">
                <img src="../assets/images/autogen/clearpixel.gif" width="4" height="1" border="0" alt="">
              </td>
              <td width="970">
                <img src="../assets/images/autogen/clearpixel.gif" width="970" height="1" border="0" alt="">
              </td>
            </tr>
            <tr valign="top" align="left">
              <td></td>
              <td width="970" class="TextObject">
                <center>

                  <p style="margin-bottom: 0px;">&nbsp;</p>

                  <h2>Termine</h2>

                  <div id="vf_calendar_list"></div>
                  <script type="text/javascript" src="https://vereinsflieger.de/calendar_ext.php?u=39093fcf8b7527d5ce93b92b99eafcce"></script>

                  <center>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <div id="TextImpress">
        <p style="font-size: 12px; color: rgb(51,51,153);">
            <a href="../html/impressum.html">Impressum</a>
            &nbsp;|&nbsp;
            <a href="../html/datenschutz.html">Datenschutz</a>
        </p>
    </div>

    <div id="BildFacebook">
        <a href="http://www.facebook.com/segelflug.berlin" target="_blank">
            <img id="Bild98" height="17" width="17" src="../assets/images/facebook.png" border="0" alt="LSC Interflug Berlin e.V. auf Facebook"
                title="LSC Interflug Berlin e.V. auf Facebook">
        </a>
    </div>
    <div id="TextFacebook" class="TextObject">
        <p style="margin-bottom: 0px;">
            <a href="http://www.facebook.com/segelflug.berlin" target="_blank">
                <span style="font-size: 9pt; color: rgb(51,51,153);">Facebook</span>
            </a>
        </p>
    </div>

    <div id="BildYoutube">
        <a href="https://www.youtube.com/user/lscinterflugberlin" target="_blank">
            <img id="Bild99" height="17" width="24" src="../assets/images/YouTube.png" border="0" alt="YouTube-Kanal des LSC Interflug Berlin e.V."
                title="YouTube-Kanal des LSC Interflug Berlin e.V.">
        </a>
    </div>
    <div id="TextYoutube" class="TextObject">
        <p style="margin-bottom: 0px;">
            <a href="https://www.youtube.com/user/lscinterflugberlin" target="_blank">
                <span style="font-size: 9pt; color: rgb(51,51,153);">YouTube</span>
            </a>
        </p>
    </div>

  </div>
</body>

</html>